const di = require('./di');
const { graphql } = require('./handlers/graphql');

const wrap = fn => (req, res) => {
  req.di = di;
  return fn(req, res);
};

module.exports = {
  graphql: wrap(graphql),
};
