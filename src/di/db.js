const admin = require('firebase-admin');
const _ = require('lodash');

const getDb = _.memoize((cfg = null) => {
  admin.initializeApp();
  return admin.firestore();
});

const getPollsRef = _.memoize(() => getDb().collection('polls'));
const getOptionsRef = pollId => getPollsRef().doc(pollId).collection('options');
const getVotesRef = pollId => getPollsRef().doc(pollId).collection('votes');

module.exports = {
  getDb,

  getPollsRef,
  getOptionsRef,
  getVotesRef,
};
