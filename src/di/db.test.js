const {
  getPollsRef,
  getOptionsRef,
  getVotesRef,
} = require('./db');

describe('getPollsRef', () => {
  test('path', () => {
    expect(getPollsRef().path)
      .toEqual('polls');
  });
});
describe('getOptionsRef', () => {
  test('path', () => {
    expect(getOptionsRef('testP').path)
      .toEqual('polls/testP/options');
  });
});
describe('getVotesRef', () => {
  test('path', () => {
    expect(getVotesRef('testP').path)
      .toEqual('polls/testP/votes');
  });
});
