const { ApolloServer, gql, SchemaDirectiveVisitor } = require('apollo-server-cloud-functions');
const axios = require('axios');
const { firestore } = require('firebase-admin');
const { defaultFieldResolver } = require('graphql');
const _ = require('lodash');

const { di: { verifyJwt } } = require('@VicPAH/jwt-utils');

const {
  getPollsRef, getOptionsRef, getVotesRef,
} = require('../di/db');

const docIdPath = () => firestore.FieldPath.documentId();

const typeDefs = gql`
directive @auth(
  audience: [String!]
  admin: Boolean
  required: Boolean
) on OBJECT | FIELD_DEFINITION

type Query
  @auth(required: false)
{
  polls: [Poll]
  pollById(id: ID): PollResp
  optionById(id: ID, pollId: ID): OptionResp
  voteById(id: ID, pollId: ID): VoteResp
    @auth(admin: true)
  voteBySelf(pollId: ID): VoteResp
    @auth(audience: ["vicpah.org.au","vicpah.org.au/guest"])

  optionQuery(group: String, query: String): [OptionQueryRes]
}
type Mutation {
  createPoll(poll: CreatePoll): PollResp
    @auth(
      audience: ["vicpah.org.au"]
      admin: true
    )
  createOption(option: CreateOption): OptionResp
    @auth(required: false, audience: ["vicpah.org.au","vicpah.org.au/guest"])
  createVote(vote: CreateVote): VoteResp
    @auth(required: false, audience: ["vicpah.org.au","vicpah.org.au/guest"])
  disableOption(pollId: ID!, optionId: ID!, reason: String): OptionResp
    @auth(
      audience: ["vicpah.org.au"]
      admin: true
    )
}

type Poll {
  id: ID!
  title: String!
  description: String
  options: [Option!]
  selfVote: Vote
}
input CreatePoll {
  title: String!
  description: String
}
type PollResp implements MutationResponse {
  success: Boolean!
  message: String!
  poll: Poll
}

type Option {
  id: ID!
  text: String!
  meta: String
  votesCount: Int!
  disabled: Boolean!
  disabledReason: String

  pollId: ID!
  poll: Poll!
  #votes: [Vote!]
}
input CreateOption {
  text: String!
  meta: String
  disabledReason: String
  pollId: ID!
}
type OptionResp implements MutationResponse {
  success: Boolean!
  message: String!
  option: Option
}

type Vote {
  id: ID!
  options: [Option!]
  optionIds: [ID!]
}
input CreateVote {
  pollId: ID!
  optionIds: [ID!]
}
type VoteResp implements MutationResponse {
  success: Boolean!
  message: String!
  vote: Vote
}

interface OptionQueryRes {
  id: ID!
  text: String!
  meta: String
}
type NetflixQueryRes implements OptionQueryRes {
  id: ID!
  text: String!
  meta: String
}

interface MutationResponse {
  success: Boolean!
  message: String!
}
type GenericResp implements MutationResponse {
  success: Boolean!
  message: String!
}
`;

class AuthDirective extends SchemaDirectiveVisitor {
  constructor(...args) {
    super(...args);
    this.ensureFieldsWrapped = this.ensureFieldsWrapped.bind(this);
    this.verify = this.verify.bind(this);
  }

  visitObject(type) {
    type._authOpts = this.args;
    this.ensureFieldsWrapped(type);
  }

  visitFieldDefinition(field, details) {
    field._authOpts = this.args;
    this.ensureFieldsWrapped(details.objectType);
  }

  ensureFieldsWrapped(objectType) {
    if (objectType._authFieldsWrapped) return;
    objectType._authFieldsWrapped = true;

    const { verify } = this;
    const fields = objectType.getFields();
    for (const field of Object.values(fields)) {
      const { resolve = defaultFieldResolver } = field;
      field.resolve = async function (...args) { // eslint-disable-line func-names
        const authOpts = field._authOpts
              || objectType._authOpts;

        if (authOpts) {
          const ctx = args[2];
          try {
            ctx.jwtPayload = await verify(ctx.req, authOpts);
          } catch (err) {
            console.error('ERR', err);
            if (authOpts.required !== false) throw err;
          }

          // TODO better admin check
          // if (authOpts.admin && ctx.jwtPayload.sub !== 'xx9sa4R0JpiXSjAYH7fs')
          //   throw new Error('Must be an administrator');
          if (authOpts.admin) throw new Error('Must be an administrator');
        }

        return resolve.apply(this, args);
      };
    }
  }

  async verify(req, opts) {
    const headerVal = req.get('authorization');
    if (!headerVal) throw new Error('No Authorization header');
    if (!headerVal.toLowerCase().startsWith('bearer ')) throw new Error('Must authorize with Bearer token');
    const token = headerVal.substr(7).trim();
    return verifyJwt({
      ...opts || {},
      token,
    });
  }
}

const serDocSnap = (snap, extra = {}) => ({
  ...snap.data(),
  ...extra,
  id: snap.id,
});
const serList = async query => {
  const { docs } = await query.get();
  return docs.map(snap => serDocSnap(snap));
};
const getDirect = async (ref, id, name) => {
  const docRef = ref.doc(id);
  const doc = await docRef.get();
  if (!doc.exists) return { success: false, message: 'Not found', [name]: null };
  return { success: true, message: 'Found', [name]: serDocSnap(doc, { ref, docRef }) };
};
// eslint-disable-next-line no-use-before-define
const get = async (name, data) => (await resolvers.Query[`${name}ById`](null, data))[name];

const getUserId = ctx => _.get(ctx, 'req.jwtPayload.sub', `unauth.${ctx.req.headers['x-vicpah-client-id']}`);

const resolvers = {
  Query: {
    pollById: (root, { id }) => getDirect(getPollsRef(), id, 'poll'),
    optionById: (root, { id, pollId }) => getDirect(getOptionsRef(pollId), id, 'option'),
    voteById: (root, { id, pollId }) => getDirect(getVotesRef(pollId), id, 'vote'),
    polls: () => serList(getPollsRef()),
    optionQuery: async (root, { group, query }) => {
      if (group !== 'netflix') throw new Error('Unknown group');
      const resp = await axios.post(
        'https://apis.justwatch.com/content/titles/en_AU/popular',
        {
          query,
          providers: ['nfx'],
          content_types: ['movie'],
          page_size: 10,
        },
      );
      return resp.data.items.map(item => ({
        id: `${item.id}`,
        text: item.title,
        meta: JSON.stringify({
          justWatchId: item.id,
          justWatchPoster: item.poster,
        }),
        __typename: 'NetflixQueryRes',
      }));
    },
  },
  Mutation: {
    createPoll: async (root, args) => {
      try {
        const ref = getPollsRef().doc();
        await ref.set(args.poll);
        return {
          success: true,
          message: 'Created poll',
          poll: serDocSnap(await ref.get()),
        };
      } catch (err) {
        console.error('Error creating poll:', err);
        return { success: false, message: 'Unknown error' };
      }
    },
    createOption: async (root, args) => {
      try {
        const pollSnap = await (getPollsRef().doc(args.option.pollId).get());
        if (!pollSnap.exists) return { success: false, message: "Poll doesn't exist" };

        const optsRef = getOptionsRef(args.option.pollId);

        const existSnap = await (optsRef.where('text', '==', args.option.text).get());
        if (!existSnap.empty) {
          if (existSnap[0].disabledReason) {
            return { success: false, message: existSnap[0].disabledReason };
          }
          return { success: false, message: 'Option already exists' };
        }

        const ref = optsRef.doc();
        await ref.set(args.option);

        return {
          success: true,
          message: 'Created option',
          option: serDocSnap(await ref.get()),
        };
      } catch (err) {
        console.error('Error creating option:', err);
        return { success: false, message: 'Unknown error' };
      }
    },
    createVote: async (root, args, ctx) => {
      try {
        const { vote: { pollId, optionIds } } = args;

        const pollSnap = await (getPollsRef().doc(pollId).get());
        if (!pollSnap.exists) return { success: false, message: `Poll ${JSON.stringify(pollId)} doesn't exist` };

        const optionIdsSet = _.uniq(optionIds);
        const optionsSnap = await (
          getOptionsRef(pollId)
            .where(docIdPath(), 'in', optionIdsSet)
            .get()
        );
        if (optionsSnap.size !== optionIdsSet.length) {
          for (const optId of optionIdsSet) {
            if (!optionsSnap.docs.find(opt => opt.id === optId)) return { success: false, message: `Option ${JSON.stringify(optId)} doesn't exist` };
          }
        }

        const votesRef = getVotesRef(args.vote.pollId);

        const ref = votesRef.doc(getUserId(ctx));
        await ref.set(args.vote);

        return {
          success: true,
          message: 'Created vote',
          vote: serDocSnap(await ref.get()),
        };
      } catch (err) {
        console.error('Error creating vote:', err);
        return { success: false, message: 'Unknown error' };
      }
    },
    disableOption: async (root, args, ctx) => {
      try {
        const { pollId, optionId, reason } = args;

        const ref = getOptionsRef(pollId).doc(optionId);
        const snap = await (ref.get());
        if (!snap.exists) return { success: false, message: `option ${JSON.stringify(pollId)}/${JSON.stringify(optionId)} doesn't exist` };

        await ref.update({ disabledReason: reason }, {});
        return {
          success: true,
          message: 'Updated disabled status',
          option: serDocSnap(await ref.get()),
        }
      } catch (err) {
        console.error('Error disabling option:', err);
        return { success: false, message: 'Unknown error' };
      }
    },
  },
  Poll: {
    options: poll => serList(getOptionsRef(poll.id)),
    selfVote: (poll, args, ctx) => get('vote', { id: getUserId(ctx), pollId: poll.id }),
  },
  Option: {
    poll: option => get('poll', { id: option.pollId }),
    // votes: option => serList(getVotesRef(option.id)),
    votesCount: async option => {
      const votesRef = getVotesRef(option.pollId);
      const snap = await votesRef.where('optionIds', 'array-contains', option.id).get();
      return snap.size;
    },
    disabled: option => !!option.disabledReason,
  },
  Vote: {
    // TODO single query... maybe vote.ref.parent.parent.where('id', 'in', vote.optionIds)
    options: vote => Promise.all(vote.optionIds.map(id => get('option', { id, pollId: vote.pollId }))),
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  schemaDirectives: {
    auth: AuthDirective,
  },
  context: c => c,
  playground: true,
  introspection: true,
});
exports.graphql = server.createHandler({
  cors: {
    allowedHeaders: [
      'authorization',
      'content-type',
      'x-vicpah-client-id',
    ],
    credentials: true,
    origin: [
      'http://localhost:3000',
      'https://www.vicpah.org.au',
    ],
  },
});
