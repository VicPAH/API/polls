locals {
  package_suffix = formatdate("YYYYMMDD-hhmmss", timestamp())
}
resource "random_id" "code_bucket" {
  byte_length = 2
  prefix      = "polls_code_"
}
resource "google_storage_bucket" "code" {
  name = random_id.code_bucket.hex
}
resource "google_storage_bucket_object" "code" {
  name         = "package-${local.package_suffix}.zip"
  bucket       = google_storage_bucket.code.name
  source       = "../package.zip"
  content_type = "application/zip"
  metadata     = {}
}


module "graphql_fn" {
  source      = "./modules/public_function"
  name        = "graphql"
  entry_point = "graphql"
  description = "GraphQL API"
  code_object = google_storage_bucket_object.code

  environment_variables = {
    member_profiles_project_id_prefix = var.member_profiles_google_project_id_prefix
  }
}
