terraform {
  backend "gcs" {
  }
}
provider "random" {
  version = "~> 2.3"
}
provider "google" {
  version = "~> 3.31"
  region  = var.region
  project = "${var.google_project_id_prefix}-${terraform.workspace}"
}
