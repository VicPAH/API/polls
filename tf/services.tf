resource "google_project_service" "appengine" {
  service = "appengine.googleapis.com"
}
resource "google_project_service" "cloudfunctions" {
  service = "cloudfunctions.googleapis.com"
}
resource "google_project_service" "cloudbuild" {
  service = "cloudbuild.googleapis.com"
}
resource "google_project_service" "firestore" {
  service = "firestore.googleapis.com"
}
