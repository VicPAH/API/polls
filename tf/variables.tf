variable "region" {
  default = "australia-southeast1"
}
variable "commit_short_sha" {
  type = string
}
variable "google_project_id_prefix" {
  type = string
}
variable "member_profiles_google_project_id_prefix" {
  type = string
}
