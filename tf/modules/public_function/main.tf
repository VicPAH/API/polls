resource "google_cloudfunctions_function" "fn" {
  name        = var.name
  description = var.description
  runtime     = var.runtime

  available_memory_mb   = 128
  source_archive_bucket = var.code_object.bucket
  source_archive_object = var.code_object.name
  trigger_http          = true
  entry_point           = var.entry_point
  timeout               = 10
  max_instances         = 5

  environment_variables = merge(
    { DEBUG = "*:error" },
    var.environment_variables,
  )
}
resource "google_cloudfunctions_function_iam_binding" "public" {
  cloud_function = google_cloudfunctions_function.fn.id
  role           = "roles/cloudfunctions.invoker"
  members        = ["allUsers"]
}
