module.exports = {
  apps: [
    {
      name: 'graphql',
      script: 'functions-framework --port=8080 --target=graphql',
      watch: ['src'],
    },
  ],
};
